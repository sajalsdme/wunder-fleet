Fleet Sample App
======================== 
This is a simple map app to show list car as marker. While tapping on a marker shows the details of the vehicle. Uses mock api with static response.

As this app usages static response from api, so the map shows in zoom level zero, so that user can see all the vehicle. Clicking on the vehicle, zoom the map to pointed marker, shows info window,  and hides all other vehicle from map. Clicking again on that vehicle take user to details view of that vehicle.

When a user click on a vehicle to see the small info, all other marker is gone. Tapping on the map or option menu `Show all` will bring back all the vehicle.

#### Possible performance optimizations for code.
- can add connection live data, and call retry function for failed api when the internet connection back

#### Things could be done better, than I’ve done it?
- Wrote test code after writing the implementation. It could be TTD approach where it could be test first approach.

## Feature List
The app displays a list of cars and details information from rest api. User can also sent a car from the details page.

* Show Map in a fragment
* show all available car as marker
* show current user live location
* Each marker item show custom info window including:
* vehicle name
* license no
* Each details car item contains:.
* avatar  (car image)
* basic info card (title, license, address)
* details info (lat, lon, city, dame, clean etc..)
* Quick Rent gives the ability to rent a car
* show a success dialog when complete
* Tapping on map shows all vehicle
* Dedicated toolbar menu to shows all vehicle
#### Technical Specification Highlight
* Usages single activity
* Custom marker info window
* Uses custom toast background when an error occur
* Uses both databinding and view binding
* Support night/day mode

## Screenshots
Landing page | All Vehicle | Car details
--- | --- | ---
![Landing page](screenshots/fleet_zoom_out.png  "Entry Screen")  | ![Landing page](screenshots/fleet.png  "Entry Screen")  | ![Landing page](screenshots/car_details.png  "Entry Screen")

Info window | Quick Rent | Rent Success
--- | --- | ---
![Landing page](screenshots/info_window.png  "Entry Screen")  | ![Landing page](screenshots/quick_rent.png  "Entry Screen")  | ![Landing page](screenshots/reservation_success.png  "Entry Screen")


#### Android Studio IDE setup
For development, the latest version of Android Studio is required. The latest version can be    
downloaded from [here](https://developer.android.com/studio/).

This project Uses [ktlint](https://ktlint.github.io/) to enforce Kotlin coding styles. This one uses a recommended plugin called  [ktlint-gradle](https://github.com/jlleitschuh/ktlint-gradle#idea-plugin-simple-setup).    
Here's how to configure it for use with Android Studio (instructions adapted    
from the ktlint [README](https://github.com/jlleitschuh/ktlint-gradle#idea-plugin-simple-setup)):

- you can run `./gradlew ktlintFormat` to format the code
- you can run `./gradlew ktlintChecks` to check the code style

- Start Android Studio

### Project setup
- Clone the project
- Import the project Android Studio
- Build and run

### Testing
- Connect the test-device or run emulator.
- Turn off the animations of test-device or emulator by going to `Developer Option`.
- Run UI test from android test directory using android studio.
- Run Unit test from test directory using android studio.

## Libraries Used

##### [Architecture component][0] - A collection of libraries that help to design robust, testable, and   maintainable apps. Start with classes for managing your UI component lifecycle and handling data persistence.
* [Data Binding][1] - Declaratively bind observable data to UI elements.
* [View Binding][2] - A feature that allows you to more easily write code that interacts with view.
* [LiveData][4] - Build data objects that notify views when the underlying database changes.
* [ViewModel][6] - Store UI-related data that isn't destroyed on app rotations. Easily schedule    
  asynchronous tasks for optimal execution.
* [Ktx][7] - Provide concise, idiomatic Kotlin code
##### UI components
* [Fragment][8] - A basic unit of composable UI.
* [Layout][9] - Lay out widgets using different algorithms.
##### SDK
* [Map SDK][11] - Provides google maps feature.
#####  Third party
* [HILT][10] for dependency injection and to abstract away boiler-plate code.
* [RXJava3][12] for enabling Reactive Programming.
* [Picasso][13] for image loading.
* [Retrofit][14] A strongly-typed, caching GraphQL client for Android.
* [GSON][20] serialization/deserialization library to convert Java Objects into JSON and back.
#####  UI and Unit Test
* [Junit][15] unit testing framework.
* [AssertJ][16] allows you to write assertions.
* [Mockito][17] mocking framework.
* [Espresso][18] Ui test framework.

[0]: https://developer.android.com/jetpack/components
[1]: https://developer.android.com/topic/libraries/data-binding
[2]:https://developer.android.com/topic/libraries/view-binding
[4]: https://developer.android.com/topic/libraries/architecture/livedata
[5]: https://developer.android.com/topic/libraries/architecture/paging
[6]: https://developer.android.com/topic/libraries/architecture/viewmodel
[7]: https://developer.android.com/kotlin/ktx

[8]: https://developer.android.com/guide/components/fragments
[9]: https://developer.android.com/guide/topics/ui/declaring-layout

[10]: https://dagger.dev/hilt/
[11]:https://developers.google.com/maps/documentation/android-sdk/start
[12]:https://github.com/ReactiveX/RxJava
[13]:https://square.github.io/picasso/
[14]:https://square.github.io/retrofit/
[15]:https://junit.org/junit4/
[16]:https://joel-costigliola.github.io/assertj/
[17]:https://site.mockito.org/
[18]:https://developer.android.com/training/testing/espresso#packages
[18]:https://junit.org/junit4/
[20]:https://github.com/google/gson