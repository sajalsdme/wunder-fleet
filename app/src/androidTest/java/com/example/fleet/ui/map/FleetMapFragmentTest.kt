package com.example.fleet.ui.map

import android.content.Context
import android.widget.TextView
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withParent
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.fleet.R
import com.example.fleet.base.JsonFileUtil
import com.example.fleet.base.launchFragmentInHiltContainer
import com.example.fleet.data.mapper.toCarsUiModel
import com.example.fleet.di.RepositoryModule
import com.example.fleet.repository.FleetRepository
import com.example.network.entity.Car
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import io.reactivex.rxjava3.core.Observable
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito

@UninstallModules(RepositoryModule::class)
@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class FleetMapFragmentTest {
    @get:Rule(order = 0)
    val hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 2)
    val taskExecutorRule = InstantTaskExecutorRule()

    @BindValue @JvmField
    val fleetRepository: FleetRepository = Mockito.mock(FleetRepository::class.java)

    private lateinit var instrumentationContext: Context

    @Before
    fun setUp() {
        hiltRule.inject()

        val type = object : TypeToken<List<Car>>() {}.type
        val carListString = JsonFileUtil.getStringFromJsonFile("CarList.json")
        val cars: List<Car> = Gson().fromJson(carListString, type)
        val carUiModels = cars.toCarsUiModel()

        Mockito.`when`(fleetRepository.getFleet()).thenReturn(Observable.just(carUiModels))

        instrumentationContext = InstrumentationRegistry.getInstrumentation().context

        // Delay to load the view
        Thread.sleep(1000)
    }

    @Test
    fun isMapFragmentVisible() {
        launchFragmentInHiltContainer<FleetMapFragment>()

        onView(withId(R.id.rootMapFragment))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkOptionMenuVisible() {
        launchFragmentInHiltContainer<FleetMapFragment>()

        Espresso.openActionBarOverflowOrOptionsMenu(instrumentationContext)
        onView(withText(R.string.action_show_all))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkToolbarTitle() {
        launchFragmentInHiltContainer<FleetMapFragment>()

        onView(allOf(instanceOf(TextView::class.java), withParent(withId(R.id.toolbar))))
            .check(matches(withText(R.string.title_map)))
    }
}
