package com.example.fleet.ui.details

import android.content.Context
import android.os.Bundle
import android.widget.TextView
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeUp
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.fleet.R
import com.example.fleet.base.JsonFileUtil
import com.example.fleet.base.launchFragmentInHiltContainer
import com.example.fleet.data.mapper.toCarDetailsUiModel
import com.example.fleet.data.mapper.toUiModel
import com.example.fleet.data.model.CarDetailsUiModel
import com.example.fleet.data.model.ReservationUiModel
import com.example.fleet.di.RepositoryModule
import com.example.fleet.repository.FleetRepository
import com.example.network.entity.CarDetails
import com.example.network.entity.Reservation
import com.google.gson.Gson
import dagger.hilt.android.testing.BindValue
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import io.reactivex.rxjava3.core.Observable
import org.hamcrest.CoreMatchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito

@UninstallModules(RepositoryModule::class)
@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class CarDetailsFragmentTest {
    @get:Rule(order = 0)
    val hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 2)
    val taskExecutorRule = InstantTaskExecutorRule()

    private lateinit var instrumentationContext: Context

    private lateinit var carDetailsUiModel: CarDetailsUiModel
    private lateinit var reservationUiModel: ReservationUiModel

    @BindValue
    @JvmField
    val fleetRepository: FleetRepository = Mockito.mock(FleetRepository::class.java)

    @Before
    fun setUp() {
        hiltRule.inject()

        val carDetailsString = JsonFileUtil.getStringFromJsonFile("CarDetails.json")
        carDetailsUiModel = Gson().fromJson(carDetailsString, CarDetails::class.java).toCarDetailsUiModel()

        val reservationString = JsonFileUtil.getStringFromJsonFile("Reservation.json")
        reservationUiModel = Gson().fromJson(reservationString, Reservation::class.java).toUiModel()

        instrumentationContext = InstrumentationRegistry.getInstrumentation().context

        Mockito.`when`(fleetRepository.getCarDetails(Mockito.anyInt()))
            .thenReturn(Observable.just(carDetailsUiModel))

        Mockito.`when`(fleetRepository.rentAVehicle(Mockito.anyInt(), Mockito.anyString()))
            .thenReturn(Observable.just(reservationUiModel))

        val bundle = Bundle()
        bundle.putInt("fleet_id", carDetailsUiModel.carId)
        launchFragmentInHiltContainer<CarDetailsFragment>(bundle)
    }

    @Test
    fun detailsFragmentOnVIew() {
        onView(withId(R.id.rootCarDetailsView))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkToolbarTitle() {
        onView(
            CoreMatchers.allOf(
                CoreMatchers.instanceOf(TextView::class.java),
                ViewMatchers.withParent(withId(R.id.carToolbar))
            )
        ).check(matches(withText(R.string.title_car_details)))
    }

    @Test
    fun checkDataOnView() {
        onView(withText("Vehicle: ${carDetailsUiModel.title}"))
            .check(matches(isDisplayed()))
        onView(withId(R.id.tvLicensePlate))
            .check(matches(withText("License: ${carDetailsUiModel.licencePlate}")))
        onView(withId(R.id.tvAddress))
            .check(matches(withText("Address: ${carDetailsUiModel.address}")))

        onView(withText(R.string.tag_car_id)).check(matches(isDisplayed()))
        onView(withText(R.string.tag_lat)).check(matches(isDisplayed()))
        onView(withText(R.string.tag_lon)).check(matches(isDisplayed()))
        onView(withText(R.string.tag_location_id)).check(matches(isDisplayed()))

        onView(withId(R.id.detailScrollView)).perform(swipeUp())

        onView(withText(R.string.tag_city)).check(matches(isDisplayed()))
        onView(withText(R.string.tag_vehicle_state_id)).check(matches(isDisplayed()))
        onView(withText(R.string.tag_vehicle_type_id)).check(matches(isDisplayed()))

        onView(withId(R.id.detailScrollView)).perform(swipeUp())

        onView(withText(R.string.tag_damaged)).check(matches(isDisplayed()))
        onView(withText(R.string.tag_damaged_des)).check(matches(isDisplayed()))
        onView(withText(R.string.pricing_parking)).check(matches(isDisplayed()))
        onView(withText(R.string.pricing_time)).check(matches(isDisplayed()))

        onView(withText(R.string.quick_rent)).check(matches(isDisplayed()))
    }

    @Test
    fun checkReservationSuccessDialog() {
        onView(withId(R.id.detailScrollView)).perform(swipeUp())
        onView(withText(R.string.quick_rent)).check(matches(isDisplayed()))

        onView(withText(R.string.quick_rent)).perform(click())

        // check if dialog is shown
        onView(withText(R.string.reservation_success))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))

        onView(withText(R.string.okay))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))

        // Clicking okay to close dialog
        onView(withText(R.string.okay)).perform(click())

        // check dialog is gone
        onView(withText(R.string.reservation_success)).check(doesNotExist())
    }
}
