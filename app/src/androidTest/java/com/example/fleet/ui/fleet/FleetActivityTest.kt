package com.example.fleet.ui.fleet

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.fleet.R
import com.example.fleet.ui.details.CarDetailsFragment
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class FleetActivityTest {

    @get:Rule(order = 0)
    val hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    val activityRule = ActivityScenarioRule(FleetActivity::class.java)

    @get:Rule(order = 2)
    val taskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        hiltRule.inject()
        // Delay to load the view
        Thread.sleep(1000)
    }

    @Test
    fun displayedMapFragment() {
        onView(withId(R.id.rootMapFragment)).check(matches(isDisplayed()))
    }

    @Test
    fun canAddDetailsFragment() {
        showDetailsFragment()
        onView(withId(R.id.rootCarDetailsView))
            .check(matches(isDisplayed()))
    }

    @Test
    fun pressBackOnDetailsFragmentShowMapFragment() {
        showDetailsFragment()
        onView(withId(R.id.rootCarDetailsView))
            .check(matches(isDisplayed()))

        pressBack()

        onView(withId(R.id.rootMapFragment))
            .check(matches(isDisplayed()))
    }

    @Test
    fun pressBackCloses_App() {
        Espresso.pressBackUnconditionally()
        activityRule.scenario.close()

        assert(activityRule.scenario.state == Lifecycle.State.DESTROYED)
    }

    private fun showDetailsFragment() {
        val carDetailsFragment = CarDetailsFragment.newInstance(1)

        activityRule.scenario.onActivity {
            it.addFragment(
                rootLayoutId = R.id.fragment_container_view,
                fragment = carDetailsFragment,
                addToBackStack = true
            )
        }

        Thread.sleep(1000)
    }
}
