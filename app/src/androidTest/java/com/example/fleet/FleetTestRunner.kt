package com.example.fleet

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import com.example.fleet.base.HiltTestWunderFleet_Application

class FleetTestRunner : AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader?, className: String?, context: Context?): Application {
        return super.newApplication(cl, HiltTestWunderFleet_Application::class.java.name, context)
    }
}
