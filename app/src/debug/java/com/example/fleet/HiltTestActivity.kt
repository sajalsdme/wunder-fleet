package com.example.fleet

import com.example.fleet.base.activity.BaseActivity
import com.example.fleet.base.fragment.FragmentCommunicator

class HiltTestActivity : BaseActivity(), FragmentCommunicator
