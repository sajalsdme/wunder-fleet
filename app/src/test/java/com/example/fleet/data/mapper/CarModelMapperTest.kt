package com.example.fleet.data.mapper

import com.example.fleet.JsonFileUtil.getStringFromJsonFile
import com.example.fleet.data.model.CarDetailsUiModel
import com.example.fleet.data.model.CarUiModel
import com.example.fleet.data.model.ReservationUiModel
import com.example.fleet.utils.DateUtils
import com.example.network.entity.Car
import com.example.network.entity.CarDetails
import com.example.network.entity.Reservation
import com.google.gson.Gson
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class CarModelMapperTest {
    @Test
    fun toCarUiModel_returns_carUiModel() {
        val carString = getStringFromJsonFile("SingleCar.json")
        val car = Gson().fromJson(carString, Car::class.java)

        val carUiModel = car.toCarUiModel()

        assertThat(carUiModel).isInstanceOf(CarUiModel::class.java)
        assertThat(carUiModel.carId).isEqualTo(car.carId)
        assertThat(carUiModel.name).isEqualTo(if (car.title.isNullOrEmpty()) "Unknown" else car.title)
        assertThat(carUiModel.lat).isEqualTo(car.lat)
        assertThat(carUiModel.lon).isEqualTo(car.lon)

        assertThat(carUiModel.isVisible).isEqualTo(true)
        assertThat(carUiModel.clickCount).isEqualTo(0)
    }

    @Test
    fun toCarDetailsUiModel() {
        val carDetailsString = getStringFromJsonFile("CarDetails.json")
        val car = Gson().fromJson(carDetailsString, CarDetails::class.java)

        val carDetailsUiModel = car.toCarDetailsUiModel()

        assertThat(carDetailsUiModel).isInstanceOf(CarDetailsUiModel::class.java)
        assertThat(carDetailsUiModel.title).isEqualTo(if (car.title.isNullOrEmpty()) "Unknown" else car.title)
        assertThat(carDetailsUiModel.carId).isEqualTo(car.carId)
        assertThat(carDetailsUiModel.fuelLevel).isEqualTo(car.fuelLevel)
    }

    @Test
    fun toReservationUiModel() {
        val reservationString = getStringFromJsonFile("Reservation.json")
        val reservation = Gson().fromJson(reservationString, Reservation::class.java)

        val reservationUiModel = reservation.toUiModel()

        assertThat(reservationUiModel).isInstanceOf(ReservationUiModel::class.java)
        assertThat(reservationUiModel.reservationId).isEqualTo(reservation.reservationId)
        assertThat(reservationUiModel.startAddress).isEqualTo(reservation.startAddress)
        assertThat(reservationUiModel.startTime).isEqualTo(
            DateUtils.dateToString(
                reservation.startTime.toLong(),
                DateUtils.format_dd_mmm_yy
            )
        )
        assertThat(reservationUiModel.endTime).isEqualTo(
            DateUtils.dateToString(
                reservation.endTime.toLong(),
                DateUtils.format_dd_mmm_yy
            )
        )
    }
}
