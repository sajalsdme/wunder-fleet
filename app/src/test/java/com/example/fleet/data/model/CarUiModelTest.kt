package com.example.fleet.data.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class CarUiModelTest {
    private val carId = 1
    private val title = "Manfred"
    private val licencePlate = "FBL 081"
    private val lat = 51.5156
    private val lon = 7.4647

    private lateinit var carUiModel: CarUiModel

    @Before
    fun setUp() {
        carUiModel = CarUiModel(
            carId = carId,
            licensePlateNo = licencePlate,
            name = title,
            lat = lat,
            lon = lon
        )
    }

    @Test
    fun incrementCounter_Increments_clickCount() {
        carUiModel.incrementCounter()
        assertThat(carUiModel.clickCount).isEqualTo(1)
        carUiModel.incrementCounter()
        assertThat(carUiModel.clickCount).isEqualTo(2)
    }

    @Test
    fun resetCounter_resets_clickCount() {
        carUiModel.incrementCounter()
        carUiModel.incrementCounter()
        carUiModel.incrementCounter()
        assertThat(carUiModel.clickCount).isEqualTo(3)
        carUiModel.resetCounter()
        assertThat(carUiModel.clickCount).isEqualTo(0)
    }

    @Test
    fun isFirstClick_true_whenClickCount0() {
        assertThat(carUiModel.isFirstCLick()).isEqualTo(true)

        carUiModel.incrementCounter()
        carUiModel.incrementCounter()
        assertThat(carUiModel.isFirstCLick()).isEqualTo(false)

        carUiModel.resetCounter()
        assertThat(carUiModel.isFirstCLick()).isEqualTo(true)
    }

    @Test
    fun isVisible_value_toggle() {
        assertThat(carUiModel.isVisible).isEqualTo(true)
        carUiModel.isVisible = false
        assertThat(carUiModel.isVisible).isEqualTo(false)
    }
}
