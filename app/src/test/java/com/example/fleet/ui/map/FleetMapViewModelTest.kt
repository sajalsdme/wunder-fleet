package com.example.fleet.ui.map

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.fleet.JsonFileUtil
import com.example.fleet.data.mapper.toCarsUiModel
import com.example.fleet.data.model.CarUiModel
import com.example.fleet.observeOnce
import com.example.fleet.repository.FleetRepository
import com.example.network.base.RequestException
import com.example.network.entity.Car
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.rxjava3.android.plugins.RxAndroidPlugins
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.Schedulers
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

class FleetMapViewModelTest {
    private lateinit var fleetMapViewModel: FleetMapViewModel

    private val mockRepository: FleetRepository = Mockito.mock(FleetRepository::class.java)

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setErrorHandler { throwable: Throwable? -> throwable?.printStackTrace() }

        fleetMapViewModel = FleetMapViewModel(mockRepository)
    }

    @Test
    fun getFleetCar_success() {
        val type = object : TypeToken<List<Car>>() {}.type
        val carListString = JsonFileUtil.getStringFromJsonFile("CarList.json")
        val cars: List<Car> = Gson().fromJson(carListString, type)
        val carUiModels = cars.toCarsUiModel()

        Mockito.`when`(mockRepository.getFleet()).thenReturn(Observable.just(carUiModels))

        fleetMapViewModel.getFleetCars()

        // Observe car list and assert user list value
        fleetMapViewModel.carsLiveData.observeOnce {
            assertThat(it.size).isEqualTo(cars.size)
        }

        // Observe loader
        fleetMapViewModel.loaderLiveData.observeOnce {
            assertThat(it).isEqualTo(false)
        }

        // Error message loader
        fleetMapViewModel.errorMessageLiveData.observeOnce {
            assertThat(it).isEqualTo(null)
        }
    }

    @Test
    fun getFleetCar_empty() {
        val carUiModels = ArrayList<CarUiModel>()
        Mockito.`when`(mockRepository.getFleet()).thenReturn(Observable.just(carUiModels))

        fleetMapViewModel.getFleetCars()

        // Observe car list and assert user list value
        fleetMapViewModel.carsLiveData.observeOnce {
            assertThat(it).isNullOrEmpty()
        }

        // Observe loader
        fleetMapViewModel.loaderLiveData.observeOnce {
            assertThat(it).isEqualTo(false)
        }

        // Error message loader
        fleetMapViewModel.errorMessageLiveData.observeOnce {
            assertThat(it).isEqualTo(null)
        }
    }

    @Test
    @Throws(RequestException::class)
    fun getFleetCar_Error() {
        val unauthorizedErrorMessage = "Unauthorized Exception"
        val requestException = RequestException(401, unauthorizedErrorMessage)

        Mockito.`when`(mockRepository.getFleet()).thenReturn(Observable.error(requestException))
        try {
            // getFleetCar called at time of initialization
            fleetMapViewModel.getFleetCars()
        } catch (exception: RequestException) {
            // Observe car list and assert user list value
            fleetMapViewModel.carsLiveData.observeOnce {
                assertThat(it).isNull()
            }

            // Observe loader
            fleetMapViewModel.loaderLiveData.observeOnce {
                assertThat(it).isEqualTo(false)
            }

            // Error message loader
            fleetMapViewModel.errorMessageLiveData.observeOnce {
                assertThat(it).isInstanceOf(RequestException::class.java)
            }
        }
    }
}
