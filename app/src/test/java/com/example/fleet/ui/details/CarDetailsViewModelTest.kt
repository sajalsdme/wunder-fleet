package com.example.fleet.ui.details

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.fleet.JsonFileUtil
import com.example.fleet.data.mapper.toCarDetailsUiModel
import com.example.fleet.data.mapper.toUiModel
import com.example.fleet.observeOnce
import com.example.fleet.repository.FleetRepository
import com.example.network.base.RequestException
import com.example.network.entity.CarDetails
import com.example.network.entity.Reservation
import com.google.gson.Gson
import io.reactivex.rxjava3.android.plugins.RxAndroidPlugins
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.Schedulers
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

class CarDetailsViewModelTest {
    private lateinit var carDetailsViewModel: CarDetailsViewModel

    private val mockRepository: FleetRepository = Mockito.mock(FleetRepository::class.java)

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setErrorHandler { throwable: Throwable? -> throwable?.printStackTrace() }

        carDetailsViewModel = CarDetailsViewModel(mockRepository, "")
    }

    @Test
    fun getFleetDetails_success() {
        val carDetailsString = JsonFileUtil.getStringFromJsonFile("CarDetails.json")
        val car = Gson().fromJson(carDetailsString, CarDetails::class.java)
        val carDetailsUiModel = car.toCarDetailsUiModel()

        Mockito.`when`(mockRepository.getCarDetails(car.carId))
            .thenReturn(Observable.just(carDetailsUiModel))

        carDetailsViewModel.getFleetDetails(fleetId = car.carId)

        // Observe car details and assert value
        carDetailsViewModel.carsDetailsLiveData.observeOnce {
            assertThat(it.carId).isEqualTo(car.carId)
        }

        // Observe loader
        carDetailsViewModel.loaderLiveData.observeOnce {
            assertThat(it).isNotEqualTo(true)
        }

        // Error message loader
        carDetailsViewModel.errorMessageLiveData.observeOnce {
            assertThat(it).isEqualTo(null)
        }
    }

    @Test
    @Throws(RequestException::class)
    fun getFleetDetails_Error() {
        val carId = 1
        val unauthorizedErrorMessage = "Unauthorized Exception"
        val requestException = RequestException(401, unauthorizedErrorMessage)

        Mockito.`when`(mockRepository.getCarDetails(carId))
            .thenReturn(Observable.error(requestException))
        try {
            carDetailsViewModel.getFleetDetails(carId)
        } catch (exception: RequestException) {
            // Observe car list and assert user list value
            carDetailsViewModel.carsDetailsLiveData.observeOnce {
                assertThat(it.carId).isNull()
            }

            // Observe loader
            carDetailsViewModel.loaderLiveData.observeOnce {
                assertThat(it).isNotEqualTo(true)
            }

            // Error message loader
            carDetailsViewModel.errorMessageLiveData.observeOnce {
                assertThat(it).isInstanceOf(RequestException::class.java)
            }
        }
    }

    @Test
    fun requestToRentVehicle_success() {
        val carId = prepareCarDetails()
        carDetailsViewModel.getFleetDetails(carId)

        // Prepare reservation data
        val reservationString = JsonFileUtil.getStringFromJsonFile("Reservation.json")
        val reservation = Gson().fromJson(reservationString, Reservation::class.java)
        val reservationUiModel = reservation.toUiModel()

        Mockito.`when`(mockRepository.rentAVehicle(Mockito.anyInt(), Mockito.anyString()))
            .thenReturn(Observable.just(reservationUiModel))

        // this method depends on the fleet id on carDetailsLiveData
        carDetailsViewModel.requestToRentVehicle()

        // Observe reservation data and assert value
        carDetailsViewModel.reservationSuccess.observeOnce {
            assertThat(it.carId).isNotNull
        }

        // Observe loader
        carDetailsViewModel.loaderLiveData.observeOnce {
            assertThat(it).isNotEqualTo(true)
        }

        // Error message loader
        carDetailsViewModel.errorMessageLiveData.observeOnce {
            assertThat(it).isEqualTo(null)
        }
    }

    @Test
    @Throws(RequestException::class)
    fun requestToRentVehicle_Error() {
        val carId = prepareCarDetails()
        carDetailsViewModel.getFleetDetails(carId)

        val unauthorizedErrorMessage = "Unauthorized Exception"
        val requestException = RequestException(401, unauthorizedErrorMessage)

        Mockito.`when`(mockRepository.rentAVehicle(Mockito.anyInt(), Mockito.anyString()))
            .thenReturn(Observable.error(requestException))

        try {
            carDetailsViewModel.getFleetDetails(carId)
        } catch (exception: RequestException) {
            // Observe car list and assert user list value
            carDetailsViewModel.reservationSuccess.observeOnce {
                assertThat(it).isNull()
            }

            // Observe loader
            carDetailsViewModel.loaderLiveData.observeOnce {
                assertThat(it).isEqualTo(false)
            }

            // Error message loader
            carDetailsViewModel.errorMessageLiveData.observeOnce {
                assertThat(it).isInstanceOf(RequestException::class.java)
            }
        }
    }

    private fun prepareCarDetails(): Int {
        // Prepare car details data
        val carDetailsString = JsonFileUtil.getStringFromJsonFile("CarDetails.json")
        val car = Gson().fromJson(carDetailsString, CarDetails::class.java)
        val carDetailsUiModel = car.toCarDetailsUiModel()

        Mockito.`when`(mockRepository.getCarDetails(car.carId))
            .thenReturn(Observable.just(carDetailsUiModel))

        return car.carId
    }
}
