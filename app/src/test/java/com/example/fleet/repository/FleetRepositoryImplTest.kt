package com.example.fleet.repository

import com.example.fleet.JsonFileUtil
import com.example.fleet.data.model.CarDetailsUiModel
import com.example.fleet.data.model.ReservationUiModel
import com.example.network.base.RequestException
import com.example.network.entity.Car
import com.example.network.entity.CarDetails
import com.example.network.entity.Reservation
import com.example.network.fleets.FleetsRemoteSource
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.rxjava3.core.Single
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class FleetRepositoryImplTest {
    private lateinit var repositoryImpl: FleetRepositoryImpl

    private val remoteSource: FleetsRemoteSource = Mockito.mock(FleetsRemoteSource::class.java)

    @Before
    fun setUp() {
        repositoryImpl = FleetRepositoryImpl(remoteSource, "")
    }

    @Test
    fun getFleet_return_carList() {
        val type = object : TypeToken<List<Car>>() {}.type
        val carListString = JsonFileUtil.getStringFromJsonFile("CarList.json")

        val cars: List<Car> = Gson().fromJson(carListString, type)

        Mockito.`when`(remoteSource.getFleet())
            .thenReturn(Single.just(cars))

        repositoryImpl.getFleet()
            .test()
            .assertNoErrors()
            .assertComplete()
            .assertValue { carUiModels ->
                assertThat(carUiModels.size).isEqualTo(10)
                return@assertValue true
            }
    }

    @Test
    @Throws(RequestException::class)
    fun getFleet_handles_error() {
        val unauthorizedErrorMessage = "Unauthorized Exception"
        val requestException: Throwable = RequestException(401, unauthorizedErrorMessage)

        Mockito.`when`(remoteSource.getFleet())
            .thenReturn(Single.error(requestException))

        repositoryImpl.getFleet()
            .test()
            .assertError() { exception ->
                assertThat(exception).isInstanceOf(RequestException::class.java)
                assertThat(exception.message).isEqualTo(unauthorizedErrorMessage)

                return@assertError true
            }
    }

    @Test
    fun getCarDetails_return_carDetailsUiModel() {
        val carDetailsString = JsonFileUtil.getStringFromJsonFile("CarDetails.json")

        val carDetails: CarDetails = Gson().fromJson(carDetailsString, CarDetails::class.java)

        Mockito.`when`(remoteSource.getCarDetails(carDetails.carId))
            .thenReturn(Single.just(carDetails))

        repositoryImpl.getCarDetails(carDetails.carId)
            .test()
            .assertNoErrors()
            .assertComplete()
            .assertValue { carDetailsUiModel ->
                assertThat(carDetailsUiModel).isInstanceOf(CarDetailsUiModel::class.java)
                assertThat(carDetailsUiModel.carId).isEqualTo(carDetails.carId)
                return@assertValue true
            }
    }

    @Test
    @Throws(RequestException::class)
    fun getCarDetails_handles_error() {
        val carId = 1
        val notFoundExceptionMessage = "Not Found"
        val notFoundException: Throwable = RequestException(404, notFoundExceptionMessage)

        Mockito.`when`(remoteSource.getCarDetails((carId)))
            .thenReturn(Single.error(notFoundException))

        repositoryImpl.getCarDetails(carId)
            .test()
            .assertError() { exception ->
                assertThat(exception).isInstanceOf(RequestException::class.java)
                assertThat(exception.message).isEqualTo(notFoundExceptionMessage)

                return@assertError true
            }
    }

    @Test
    fun rentAVehicle_return_success() {
        val reservationString = JsonFileUtil.getStringFromJsonFile("Reservation.json")
        val reservation = Gson().fromJson(reservationString, Reservation::class.java)

        val carId = reservation.carId
        val token = ""

        Mockito.`when`(remoteSource.rentACar(carId, token))
            .thenReturn(Single.just(reservation))

        repositoryImpl.rentAVehicle(carId, token)
            .test()
            .assertNoErrors()
            .assertComplete()
            .assertValue { reservationUiModel ->
                assertThat(reservationUiModel).isInstanceOf(ReservationUiModel::class.java)

                return@assertValue true
            }
    }

    @Test
    @Throws(RequestException::class)
    fun rentAVehicle_handles_error() {
        val carId = 1
        val token = ""

        val networkExceptionMessage = "No internet connection"
        val networkException: Throwable = RequestException(404, networkExceptionMessage)

        Mockito.`when`(remoteSource.rentACar(carId, token))
            .thenReturn(Single.error(networkException))

        repositoryImpl.rentAVehicle(carId, token)
            .test()
            .assertError() { exception ->
                assertThat(exception).isInstanceOf(RequestException::class.java)
                assertThat(exception.message).isEqualTo(networkExceptionMessage)

                return@assertError true
            }
    }
}
