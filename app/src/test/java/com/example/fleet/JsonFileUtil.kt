package com.example.fleet

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.nio.charset.Charset

object JsonFileUtil {
    fun getStringFromJsonFile(fileName: String): String {
        return readFileToString(JsonFileUtil::class.java, "/$fileName")
    }

    private fun readFileToString(contextClass: Class<*>, streamIdentifier: String): String {
        val inputStreamReader = InputStreamReader(contextClass.getResourceAsStream(streamIdentifier)!!, Charset.defaultCharset())
        try {
            val stringBuilder = StringBuilder()
            BufferedReader(inputStreamReader).use { reader ->
                var nextLine = reader.readLine()
                while (nextLine != null) {
                    stringBuilder.append(nextLine)
                    nextLine = reader.readLine()
                }
            }
            return stringBuilder.toString()
        } catch (ioException: IOException) {
            throw ioException
        } finally {
            inputStreamReader.close()
        }
    }
}
