package com.example.fleet.data.mapper

import com.example.fleet.data.model.CarDetailsUiModel
import com.example.fleet.data.model.CarUiModel
import com.example.fleet.data.model.ReservationUiModel
import com.example.fleet.utils.DateUtils
import com.example.network.entity.Car
import com.example.network.entity.CarDetails
import com.example.network.entity.Reservation

fun Car.toCarUiModel(): CarUiModel {
    return CarUiModel(
        carId = carId,
        name = if (title.isNullOrEmpty()) "Unknown" else title!!,
        licensePlateNo = licencePlate,
        lat = lat,
        lon = lon,
    )
}

fun List<Car>.toCarsUiModel(): List<CarUiModel> {
    return this.map { car ->
        car.toCarUiModel()
    }.toList()
}

fun CarDetails.toCarDetailsUiModel(): CarDetailsUiModel {
    return CarDetailsUiModel(
        carId = carId,
        title = if (title.isNullOrEmpty()) "Unknown" else title!!,
        licencePlate = licencePlate,
        address = address,
        city = city,
        damageDescription = damageDescription,
        fuelLevel = fuelLevel,
        hardwareId = hardwareId,
        isActivatedByHardware = isActivatedByHardware,
        isClean = isClean,
        isDamaged = isDamaged,
        lat = lat,
        lon = lon,
        locationId = locationId,
        pricingParking = pricingParking,
        pricingTime = pricingTime,
        reservationState = reservationState,
        vehicleStateId = vehicleStateId,
        vehicleTypeId = vehicleTypeId,
        vehicleTypeImageUrl = vehicleTypeImageUrl,
        zipCode = zipCode
    )
}

fun Reservation.toUiModel(): ReservationUiModel {
    return ReservationUiModel(
        carId = carId,
        reservationId = reservationId,
        startAddress = startAddress,
        startTime = DateUtils.dateToString(startTime.toLong(), DateUtils.format_dd_mmm_yy),
        endTime = DateUtils.dateToString(endTime.toLong(), DateUtils.format_dd_mmm_yy),
    )
}
