package com.example.fleet.data.model

import java.lang.StringBuilder

data class ReservationUiModel(
    val carId: Int,
    val reservationId: Int,
    val startAddress: String,
    val startTime: String,
    val endTime: String,
) {
    fun getSuccessMessage(): String {
        val stringBuilder: StringBuilder = StringBuilder()
        stringBuilder.append("Car Id: $carId")
        stringBuilder.append("\nReservation Id: $reservationId")
        stringBuilder.append("\nStart Address: $reservationId")
        stringBuilder.append("\nStart time: $endTime")
        stringBuilder.append("\nEnd time: $endTime")

        return stringBuilder.toString()
    }
}
