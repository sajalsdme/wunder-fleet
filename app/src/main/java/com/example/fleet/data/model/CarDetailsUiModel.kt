package com.example.fleet.data.model

data class CarDetailsUiModel(
    val carId: Int,
    val title: String,
    val licencePlate: String,
    val address: String,
    val city: String,
    val damageDescription: String,
    val fuelLevel: Int,
    val hardwareId: String,
    val isActivatedByHardware: Boolean,
    val isClean: Boolean,
    val isDamaged: Boolean,
    val lat: Double,
    val lon: Double,
    val locationId: Int,
    val pricingParking: String,
    val pricingTime: String,
    val reservationState: Int,
    val vehicleStateId: Int,
    val vehicleTypeId: Int,
    val vehicleTypeImageUrl: String,
    val zipCode: String
)
