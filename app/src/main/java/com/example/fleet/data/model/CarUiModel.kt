package com.example.fleet.data.model

data class CarUiModel(
    val carId: Int,
    val name: String,
    val licensePlateNo: String,
    val lat: Double,
    val lon: Double,
) {
    var isVisible: Boolean = true

    var clickCount: Int = 0
        private set

    fun incrementCounter() = clickCount++

    fun resetCounter() {
        clickCount = 0
    }

    fun isFirstCLick(): Boolean = clickCount == 0
}
