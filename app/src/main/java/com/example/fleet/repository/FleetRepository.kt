package com.example.fleet.repository

import com.example.fleet.data.model.CarDetailsUiModel
import com.example.fleet.data.model.CarUiModel
import com.example.fleet.data.model.ReservationUiModel
import io.reactivex.rxjava3.core.Observable

interface FleetRepository {
    fun getFleet(): Observable<List<CarUiModel>>
    fun getCarDetails(fleetId: Int): Observable<CarDetailsUiModel>
    fun rentAVehicle(fleetId: Int, token: String): Observable<ReservationUiModel>
}
