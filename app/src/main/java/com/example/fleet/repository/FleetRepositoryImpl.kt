package com.example.fleet.repository

import com.example.fleet.data.mapper.toCarDetailsUiModel
import com.example.fleet.data.mapper.toCarsUiModel
import com.example.fleet.data.mapper.toUiModel
import com.example.fleet.data.model.CarDetailsUiModel
import com.example.fleet.data.model.CarUiModel
import com.example.fleet.data.model.ReservationUiModel
import com.example.network.fleets.FleetsRemoteSource
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class FleetRepositoryImpl @Inject constructor(
    private val fleetsRemoteSource: FleetsRemoteSource,
    private val token: String
) :
    FleetRepository {
    override fun getFleet(): Observable<List<CarUiModel>> {
        val a = fleetsRemoteSource.getFleet()
            .map { cars ->
                cars.toCarsUiModel()
            }.toObservable()
        return a
    }

    override fun getCarDetails(fleetId: Int): Observable<CarDetailsUiModel> {
        return fleetsRemoteSource.getCarDetails(fleetId)
            .map { carDetail ->
                return@map carDetail.toCarDetailsUiModel()
            }.toObservable()
    }

    override fun rentAVehicle(fleetId: Int, token: String): Observable<ReservationUiModel> {
        return fleetsRemoteSource.rentACar(fleetId, token)
            .map { reservationResponse ->
                return@map reservationResponse.toUiModel()
            }.toObservable()
    }
}
