package com.example.fleet.ui.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Looper
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng

class LocationProvider(private val context: Context) : LiveData<LatLng>() {
    val TAG = javaClass.simpleName

    companion object {
        const val LOCATION_UPDATE_INTERVAL: Long = 60000L
        const val FASTEST_UPDATE_INTERVAL: Long = 10000L
        const val SMALLEST_DISPLACEMENT_METERS: Float = 10F
    }

    private val defaultLocation = LatLng(51.1657, 10.4515)

    private var locationPermissionGranted = false
    private var fusedLocationProviderClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult?.lastLocation?.let { userLocation: Location ->
                val currentLatLng = LatLng(userLocation.latitude, userLocation.longitude)
                postValue(currentLatLng)
            } ?: run {
                // if location update goes wrong
                getLastKnownLocation()
            }
        }
    }

    override fun onActive() {
        super.onActive()
        locationPermissionGranted = ContextCompat.checkSelfPermission(
            context.applicationContext,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED

        if (!locationPermissionGranted) {
            postValue(defaultLocation)
            return
        }

        startLocationUpdate()
    }

    override fun onInactive() {
        super.onInactive()
        stopCollectingLocation()
    }

    fun startCollectingLocation() {
        onActive()
    }

    fun stopCollectingLocation() {
        Log.i(TAG, "Stopping location update")
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdate() {
        if (!locationPermissionGranted) return

        Log.i(TAG, "Starting location update")

        val locationRequest = LocationRequest.create().apply {
            interval = LOCATION_UPDATE_INTERVAL
            fastestInterval = FASTEST_UPDATE_INTERVAL
            smallestDisplacement = SMALLEST_DISPLACEMENT_METERS
        }

        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )
    }

    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation() {
        fusedLocationProviderClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                // Got last known location. In some rare situations this can be null.
                location?.let {
                    val currentLatLng = LatLng(it.latitude, it.longitude)
                    postValue(currentLatLng)
                }
            }.addOnFailureListener {
                it.printStackTrace()
                postValue(defaultLocation)
            }
    }
}
