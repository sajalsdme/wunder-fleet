package com.example.fleet.ui.map

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.fleet.base.viewmodel.BaseViewModel
import com.example.fleet.data.model.CarUiModel
import com.example.fleet.repository.FleetRepository
import com.example.fleet.utils.SingleLiveEvent
import com.example.fleet.utils.withScheduler
import com.google.android.gms.maps.model.Marker
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FleetMapViewModel @Inject constructor(private val fleetRepository: FleetRepository) :
    BaseViewModel() {

    lateinit var locationProvider: LocationProvider

    private val _carListMutableLiveData = MutableLiveData<List<CarUiModel>>()
    val carsLiveData: LiveData<List<CarUiModel>> = _carListMutableLiveData

    // Used to navigate to details fragment
    val detailsFragmentsListener: SingleLiveEvent<Int> = SingleLiveEvent()

    val markerList: ArrayList<Marker> = ArrayList()

    fun initLocationProvider(context: Context) {
        locationProvider = LocationProvider(context)
    }

    fun startLocationUpdate() {
        if (::locationProvider.isInitialized) {
            locationProvider.startCollectingLocation()
        }
    }

    fun getFleetCars() {
        if (!carsLiveData.value.isNullOrEmpty()) {
            // call the api only if the car list is empty
            return
        }

        fleetRepository.getFleet()
            .withScheduler()
            .doOnSubscribe { showLoader() }
            .doFinally { hideLoader() }
            .subscribe({
                _carListMutableLiveData.postValue(it)
            }, { handleError(it) })
            .autoDispose()
    }

    fun onMarkerCLick(marker: Marker) {
        val carUiModel = marker.tag as CarUiModel

        if (carUiModel.isFirstCLick()) {
            hideAllMarkerExcept(marker)
        } else {
            // take to another fragment
            detailsFragmentsListener.postValue(carUiModel.carId)
        }
    }

    private fun hideAllMarkerExcept(selectedMarker: Marker) {
        val selectedCarUiModel = selectedMarker.tag as CarUiModel
        selectedCarUiModel.incrementCounter()
        selectedMarker.tag = selectedCarUiModel

        markerList.map { marker ->
            val carUiModel = marker.tag as CarUiModel
            if (carUiModel.carId != selectedCarUiModel.carId) {
                carUiModel.resetCounter()
                carUiModel.isVisible = false
            }
            marker.tag = carUiModel
            marker.isVisible = carUiModel.isVisible
        }
    }

    fun showAllMarker() {
        markerList.map { marker ->
            val carUiModel = marker.tag as CarUiModel
            carUiModel.resetCounter()
            carUiModel.isVisible = true

            marker.tag = carUiModel
            marker.isVisible = carUiModel.isVisible
            marker.hideInfoWindow()
        }
    }
}
