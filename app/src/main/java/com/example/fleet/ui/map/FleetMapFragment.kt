package com.example.fleet.ui.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.fleet.R
import com.example.fleet.base.fragment.ViewBindingFragment
import com.example.fleet.data.model.CarUiModel
import com.example.fleet.databinding.FragmentFleetsMapBinding
import com.example.fleet.ui.details.CarDetailsFragment
import com.example.fleet.ui.map.adapter.CarInfoWindowAdapter
import com.example.fleet.utils.errorToast
import com.example.fleet.utils.getMessageFromException
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

class FleetMapFragment :
    ViewBindingFragment<FragmentFleetsMapBinding>(),
    OnMapReadyCallback,
    GoogleMap.OnMarkerClickListener {
    val viewModel: FleetMapViewModel by viewModels()

    private var map: GoogleMap? = null
    private var locationPermissionGranted = false
    private lateinit var requestPermissionLauncher: ActivityResultLauncher<String>

    private lateinit var carInfoWindowAdapter: CarInfoWindowAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        requestPermissionLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
                if (isGranted) {
                    locationPermissionGranted = true
                }
                updateCurrentLocationUi()
            }
    }

    override fun initialize(savedInstanceState: Bundle?) {
        viewModel.getFleetCars()
        viewModel.initLocationProvider(requireContext())

        carInfoWindowAdapter = CarInfoWindowAdapter(layoutInflater)

        val mapFragment =
            childFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)

        setUpToolbar()
        subscribeToData()
    }

    private fun setUpToolbar() {
        binding.toolbar.apply {
            title = getString(R.string.title_map)
            inflateMenu(R.menu.map_menu)
            setOnMenuItemClickListener {
                if (it.itemId == R.id.action_show_all) {
                    viewModel.showAllMarker()
                }

                return@setOnMenuItemClickListener true
            }
        }
    }

    private fun subscribeToData() {
        viewModel.locationProvider.observe(
            viewLifecycleOwner,
            Observer { latLon ->
                updateCamera(latLon)
            }
        )

        viewModel.loaderLiveData.observe(
            viewLifecycleOwner,
            Observer { shouldShow ->
                if (shouldShow) fragmentCommunicator.showLoader() else fragmentCommunicator.hideLoader()
            }
        )

        viewModel.errorMessageLiveData.observe(
            viewLifecycleOwner,
            Observer { throwable ->
                requireActivity().errorToast(
                    getMessageFromException(throwable, requireContext()),
                    Toast.LENGTH_LONG
                )
            }
        )

        viewModel.detailsFragmentsListener.observe(
            viewLifecycleOwner,
            Observer { fleetId ->
                fragmentCommunicator.addFragment(
                    rootLayoutId = R.id.fragment_container_view,
                    CarDetailsFragment.newInstance(fleetId),
                    addToBackStack = true,
                )
            }
        )

        AppCompatDelegate.getDefaultNightMode()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.map = googleMap
        googleMap.setOnMapClickListener { viewModel.showAllMarker() }
        googleMap.setOnMarkerClickListener(this)
        googleMap.setInfoWindowAdapter(carInfoWindowAdapter)

        getLocationPermission()
        updateCurrentLocationUi()
        if (isNightMode()) {
            setMapDarkStyle()
        }

        viewModel.carsLiveData.observe(
            viewLifecycleOwner,
            Observer { cars ->
                prepareMarker(cars)
            }
        )
    }

    private fun setMapDarkStyle(): Boolean {
        return map?.setMapStyle(
            MapStyleOptions(
                resources
                    .getString(R.string.style_json)
            )
        ) ?: false
    }

    private fun isNightMode(): Boolean {
        val nightModeFlags: Int =
            resources.configuration.uiMode.and(Configuration.UI_MODE_NIGHT_MASK)
        return nightModeFlags == Configuration.UI_MODE_NIGHT_YES
    }

    private fun prepareMarker(cars: List<CarUiModel>) {
        viewModel.markerList.clear()

        cars.forEach { car ->
            val marker = map?.addMarker(
                MarkerOptions().position(LatLng(car.lat, car.lon))
                    .title(car.name)
                    .visible(true)
            )
            marker?.let {
                it.tag = car
                viewModel.markerList.add(marker)
            }
        }
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        viewModel.onMarkerCLick(marker)

        map?.cameraPosition?.zoom?.let { currentZoomLevel ->
            Log.i(TAG, "current zoom $currentZoomLevel")
            if (currentZoomLevel < CLOSER_ZOOM) {
                updateCamera(marker.position, CLOSER_ZOOM)
            }
        }

        return false
    }

    private fun updateCurrentLocationUi() {
        Log.i(TAG, "updateLocationUI $locationPermissionGranted")
        map?.let {
            when {
                locationPermissionGranted -> {
                    enableMyLocation()
                    viewModel.startLocationUpdate()
                }

                else -> disableMyLocation()
            }
        }
    }

    private fun updateCamera(latLng: LatLng, zoom: Float = DEFAULT_ZOOM) {
        Log.i(TAG, "updateCamera $zoom")
        map?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
    }

    @SuppressLint("MissingPermission")
    private fun enableMyLocation() {
        if (locationPermissionGranted) {
            map?.isMyLocationEnabled = true
            map?.uiSettings?.isMyLocationButtonEnabled = true
        }
    }

    @SuppressLint("MissingPermission")
    private fun disableMyLocation() {
        if (locationPermissionGranted) {
            map?.isMyLocationEnabled = true
            map?.uiSettings?.isMyLocationButtonEnabled = true
        }
    }

    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                requireContext().applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermissionGranted = true
        } else {
            requestPermissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = FleetMapFragment()

        const val DEFAULT_ZOOM: Float = 5f
        const val CLOSER_ZOOM: Float = 16.5f
    }

    override fun inflateLayout(
        layoutInflater: LayoutInflater,
        viewGroup: ViewGroup?,
        attachToRoot: Boolean
    ): FragmentFleetsMapBinding =
        FragmentFleetsMapBinding.inflate(layoutInflater, viewGroup, attachToRoot)
}
