package com.example.fleet.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.fleet.base.viewmodel.BaseViewModel
import com.example.fleet.data.model.CarDetailsUiModel
import com.example.fleet.data.model.ReservationUiModel
import com.example.fleet.repository.FleetRepository
import com.example.fleet.utils.SingleLiveEvent
import com.example.fleet.utils.TOKEN_NAMED
import com.example.fleet.utils.withScheduler
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import javax.inject.Named

@HiltViewModel
class CarDetailsViewModel @Inject constructor(
    private val fleetRepository: FleetRepository,
    @Named(TOKEN_NAMED)
    private val token: String
) :
    BaseViewModel() {
    private val _carDetailsMutableLiveData = MutableLiveData<CarDetailsUiModel>()
    val carsDetailsLiveData: LiveData<CarDetailsUiModel> = _carDetailsMutableLiveData

    val reservationSuccess = SingleLiveEvent<ReservationUiModel>()

    fun getFleetDetails(fleetId: Int) {
        if (carsDetailsLiveData.value != null) {
            // call the api if the car details is not fetched already
            return
        }

        fleetRepository.getCarDetails(fleetId)
            .withScheduler()
            .doOnSubscribe { showLoader() }
            .doFinally { hideLoader() }
            .subscribe({
                _carDetailsMutableLiveData.postValue(it)
            }, { onRequestError(it) })
            .autoDispose()
    }

    fun requestToRentVehicle() {
        val fleetId = carsDetailsLiveData.value?.carId ?: -1

        if (fleetId == -1) {
            showErrorToast(Throwable("Can not rent the car now!"))
            return
        }

        fleetRepository.rentAVehicle(fleetId, token)
            .withScheduler()
            .doOnSubscribe { showLoader() }
            .doFinally { hideLoader() }
            .subscribe({
                reservationSuccess.postValue(it)
            }, { onRequestError(it) })
            .autoDispose()
    }

    private fun onRequestError(throwable: Throwable) {
        handleError(throwable)
    }
}
