package com.example.fleet.ui.fleet

import android.os.Bundle
import android.view.LayoutInflater
import com.example.fleet.base.activity.ViewBindingActivity
import com.example.fleet.databinding.ActivityFleetBinding
import com.example.fleet.ui.map.FleetMapFragment

class FleetActivity : ViewBindingActivity<ActivityFleetBinding>() {
    override fun inflateLayout(layoutInflater: LayoutInflater): ActivityFleetBinding =
        ActivityFleetBinding.inflate(layoutInflater)

    override fun initialize(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            addFragment(
                rootLayoutId = binding.fragmentContainerView.id,
                fragment = FleetMapFragment.newInstance(),
                addToBackStack = true
            )
        }
    }
}
