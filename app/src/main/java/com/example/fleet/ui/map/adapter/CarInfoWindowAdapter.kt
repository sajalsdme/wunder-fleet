package com.example.fleet.ui.map.adapter

import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.example.fleet.R
import com.example.fleet.data.model.CarUiModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker

class CarInfoWindowAdapter(layoutInflater: LayoutInflater) : GoogleMap.InfoWindowAdapter {
    val window: View = layoutInflater.inflate(R.layout.layout_car_info_window, null)

    override fun getInfoContents(marker: Marker): View? {
        return null
    }

    override fun getInfoWindow(marker: Marker): View? {
        val carUiModel: CarUiModel = marker.tag as CarUiModel

        val tvTitle: TextView = window.findViewById(R.id.title)
        val tvSnippet: TextView = window.findViewById(R.id.snippet)

        tvTitle.text = carUiModel.name
        tvSnippet.text = carUiModel.licensePlateNo

        return window
    }
}
