package com.example.fleet.ui.dialog

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.fleet.R

private const val ARG_TITLE = "title"
private const val ARG_MESSAGE = "message"

class OneButtonDialogFragment() : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val title: String = arguments?.getString(ARG_TITLE, getString(R.string.app_name)) ?: ""
        val subtitle: String = arguments?.getString(ARG_MESSAGE, "") ?: ""

        return AlertDialog.Builder(requireContext())
            .setIcon(R.drawable.ic_wunder_logo)
            .setTitle(title)
            .setMessage(subtitle)
            .setPositiveButton(
                getString(R.string.okay),
                DialogInterface.OnClickListener { dialog, _ ->
                    dialog.dismiss()
                }
            )
            .create()
    }

    fun showDialog(fragmentManager: FragmentManager) {
        val ft: FragmentTransaction = fragmentManager.beginTransaction()
        val prev: Fragment? = fragmentManager.findFragmentByTag(this.javaClass.simpleName)
        if (prev != null) {
            ft.remove(prev)
        }
        ft.addToBackStack(null)
        show(fragmentManager, this.javaClass.simpleName)
    }

    fun closeDialog() {
        dismiss()
    }

    companion object {
        fun newInstance(title: String, message: String): OneButtonDialogFragment {
            val frag = OneButtonDialogFragment()
            val args = Bundle()
            args.putString(ARG_TITLE, title)
            args.putString(ARG_MESSAGE, message)
            frag.arguments = args
            return frag
        }
    }
}
