package com.example.fleet.ui.details

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.fleet.R
import com.example.fleet.base.fragment.DataBindingFragment
import com.example.fleet.databinding.FragmentCarDetailsBinding
import com.example.fleet.ui.dialog.OneButtonDialogFragment
import com.example.fleet.utils.errorToast
import com.example.fleet.utils.getMessageFromException
import com.example.network.base.RequestException
import com.squareup.picasso.Picasso

private const val ARG_FLEET_ID = "fleet_id"

class CarDetailsFragment : DataBindingFragment<FragmentCarDetailsBinding>() {
    val viewModel: CarDetailsViewModel by viewModels()

    private var fleetId: Int? = null

    override fun setInitialValues(savedInstanceState: Bundle?) {
        dataBinding.viewModel = viewModel
        setUpToolbar()
        subscribeToData()

        dataBinding.btRent.setOnClickListener {
            viewModel.requestToRentVehicle()
        }
    }

    private fun subscribeToData() {
        viewModel.loaderLiveData.observe(
            viewLifecycleOwner,
            Observer { shouldShow ->
                if (shouldShow) fragmentCommunicator.showLoader() else fragmentCommunicator.hideLoader()
            }
        )

        viewModel.errorMessageLiveData.observe(
            viewLifecycleOwner,
            Observer { throwable ->
                requireActivity().errorToast(getMessageFromException(throwable, requireContext()), Toast.LENGTH_LONG)
                fragmentCommunicator.hideLoader()

                Log.e(TAG, throwable.toString())
                if (throwable is RequestException) {
                    // Goto previous fragment if request goes wrong
                    Log.e(TAG, throwable.toString())
                    activity?.onBackPressed()
                }
            }
        )

        viewModel.carsDetailsLiveData.observe(
            viewLifecycleOwner,
            Observer {
                // Load Car image
                Picasso.get()
                    .load(it.vehicleTypeImageUrl)
                    .placeholder(R.drawable.ic_wunder_logo)
                    .error(R.drawable.ic_wunder_logo)
                    .into(dataBinding.ivAvatar)
            }
        )

        viewModel.reservationSuccess.observe(
            viewLifecycleOwner,
            Observer {
                val dialog = OneButtonDialogFragment.newInstance(
                    title = getString(R.string.reservation_success),
                    message = it.getSuccessMessage()
                )
                dialog.showDialog(childFragmentManager)
            }
        )
    }

    private fun setUpToolbar() {
        dataBinding.carToolbar.apply {
            title = getString(R.string.title_car_details)
            setNavigationIcon(R.drawable.ic_back_white)
            setNavigationOnClickListener {
                requireActivity().onBackPressed()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            fleetId = it.getInt(ARG_FLEET_ID)
            fleetId?.let { id ->
                viewModel.getFleetDetails(id)
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(fleetId: Int) =
            CarDetailsFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_FLEET_ID, fleetId)
                }
            }
    }

    override val layoutResourceId: Int
        get() = R.layout.fragment_car_details
}
