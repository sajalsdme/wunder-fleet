package com.example.fleet.di

import com.example.fleet.utils.TOKEN_NAMED
import com.example.fleet.utils.TokenProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class LocalDataModule {
    @Singleton
    @Provides
    @Named(TOKEN_NAMED)
    fun provideToken(): String = TokenProvider.getToken()
}
