package com.example.fleet.di

import com.example.fleet.repository.FleetRepository
import com.example.fleet.repository.FleetRepositoryImpl
import com.example.fleet.utils.TOKEN_NAMED
import com.example.network.api.ApiService
import com.example.network.fleets.FleetsRemoteSource
import com.example.network.fleets.FleetsRemoteSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {
    @Singleton
    @Provides
    fun provideFleetRemoteSource(apiService: ApiService): FleetsRemoteSource =
        FleetsRemoteSourceImpl(apiService)

    @Singleton
    @Provides
    fun provideFleetRemoteRepository(
        fleetRemoteSource: FleetsRemoteSource,
        @Named(TOKEN_NAMED) token: String
    ): FleetRepository =
        FleetRepositoryImpl(fleetRemoteSource, token)
}
