package com.example.fleet.utils

object TokenProvider {
    // this token should come from data source (like local db, shared pref) after login
    private const val token = "Bearer df7c313b47b7ef87c64c0f5f5cebd6086bbb0fa"

    fun getToken(): String {
        return token
    }
}
