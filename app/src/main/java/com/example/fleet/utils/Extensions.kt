package com.example.fleet.utils

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import com.example.fleet.R
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers

fun Activity.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT): Toast {
    return Toast.makeText(this, message, duration)
        .apply {
            show()
        }
}
fun Activity.errorToast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT): Toast {
    return Toast.makeText(this, message, duration)
        .apply {
            val inflater = layoutInflater
            val layout: View = inflater.inflate(
                R.layout.layout_error_toast,
                findViewById<ViewGroup>(R.id.custom_toast_container)
            )
            layout.findViewById<AppCompatTextView>(R.id.text).text = message
            view = layout
            show()
        }
}

fun Fragment.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) =
    requireActivity().toast(message, duration)

fun <T : Any> Observable<T>.withScheduler(): Observable<T> {
    return this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}
