package com.example.fleet.utils

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

object DateUtils {
    const val format_dd_mmm_yy = "dd MMM yyyy"

    fun dateToString(millisecond: Long, format: String): String {
        var dateStr = ""
        val formatter = SimpleDateFormat(format, Locale.US)
        try {
            val date = Date(millisecond * 1000)
            dateStr = formatter.format(date)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return dateStr
    }
}
