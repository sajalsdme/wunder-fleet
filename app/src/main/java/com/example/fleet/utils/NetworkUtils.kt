package com.example.fleet.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import com.example.fleet.R
import com.example.network.base.RequestException
import com.example.network.base.UnknownException

fun isNetworkAvailable(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val network: Network? = connectivityManager.activeNetwork
        val capabilities: NetworkCapabilities? = connectivityManager.getNetworkCapabilities(network)
        capabilities != null && (
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
            )
    } else {
        val activeNetworkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
        activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}

fun getMessageFromException(exception: Throwable?, context: Context): String {
    return when (exception) {
        is RequestException -> exception.message
        is UnknownException -> context.getString(R.string.msg_failed)
        else -> {
            if (isNetworkAvailable(context.applicationContext)) {
                context.getString(R.string.msg_unknown_exception)
            } else {
                context.getString(R.string.msg_no_network)
            }
        }
    }
}
