package com.example.fleet.base.fragment

import android.content.Intent
import androidx.annotation.AnimRes
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import com.example.fleet.R

interface FragmentCommunicator {
    fun showToast(text: String?)

    fun showLoader()

    fun hideLoader()

    fun showSnackBar(message: String)

    fun addFragment(
        @IdRes rootLayoutId: Int,
        fragment: Fragment,
        tag: String = fragment.javaClass.name,
        addToBackStack: Boolean = false,
        @AnimRes enterAnim: Int = R.anim.fade_in,
        @AnimRes exitAnim: Int = R.anim.fade_out,
    )

    fun startActivity(
        intent: Intent,
        finishSelf: Boolean,
        @AnimRes enterAnim: Int = R.anim.slide_in_right,
        @AnimRes exitAnim: Int = R.anim.slide_out_left,
    )

    fun animateStartActivity(
        @AnimRes enterAnim: Int = R.anim.slide_in_right,
        @AnimRes exitAnim: Int = R.anim.slide_out_left
    )

    fun animateEndActivity(
        @AnimRes enterAnim: Int = R.anim.slide_in_left,
        @AnimRes exitAnim: Int = R.anim.slide_out_right
    )
}
