package com.example.fleet.base.activity

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.example.fleet.R
import com.example.fleet.base.fragment.FragmentCommunicator
import com.example.fleet.utils.toast
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
abstract class BaseActivity : AppCompatActivity(), FragmentCommunicator {
    protected val TAG = this.javaClass.simpleName

    override fun showToast(text: String?) {
        text?.let { msg ->
            runOnUiThread {
                toast(msg)
            }
        }
    }

    override fun showSnackBar(message: String) {
        findViewById<View>(R.id.content)?.let {
            Snackbar.make(it, message, Snackbar.LENGTH_LONG).show()
        }
    }

    private val loaderDialog: AlertDialog by lazy {
        val builder = MaterialAlertDialogBuilder(this@BaseActivity, R.style.LoaderDialog)

        val dialogView = LayoutInflater.from(applicationContext)
            .inflate(R.layout.loading, findViewById(android.R.id.content), false)

        builder.setView(dialogView)
        builder.setCancelable(false)

        return@lazy builder.create()
    }

    override fun showLoader() {
        runOnUiThread { if (!loaderDialog.isShowing) loaderDialog.show() }
    }

    override fun hideLoader() {
        runOnUiThread { if (loaderDialog.isShowing) loaderDialog.dismiss() }
    }

    override fun addFragment(
        rootLayoutId: Int,
        fragment: Fragment,
        tag: String,
        addToBackStack: Boolean,
        enterAnim: Int,
        exitAnim: Int
    ) {
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            setCustomAnimations(enterAnim, exitAnim)
            add(rootLayoutId, fragment)
            if (addToBackStack) {
                addToBackStack(tag)
            }
        }
    }

    override fun animateStartActivity(enterAnim: Int, exitAnim: Int) {
        overridePendingTransition(enterAnim, exitAnim)
    }

    override fun animateEndActivity(enterAnim: Int, exitAnim: Int) {
        overridePendingTransition(enterAnim, exitAnim)
    }

    override fun startActivity(intent: Intent, finishSelf: Boolean, enterAnim: Int, exitAnim: Int) {
        startActivity(intent)
        animateStartActivity(enterAnim, exitAnim)
        if (finishSelf) {
            finish()
        }
    }
}
