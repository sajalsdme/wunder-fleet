package com.example.fleet.base.viewmodel

import androidx.lifecycle.ViewModel
import com.example.fleet.utils.SingleLiveEvent
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable

abstract class BaseViewModel : ViewModel() {
    val TAG = javaClass.simpleName

    private val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun Disposable.autoDispose() = compositeDisposable.add(this)

    val loaderLiveData by lazy { SingleLiveEvent<Boolean>() }
    val errorMessageLiveData by lazy { SingleLiveEvent<Throwable>() }
    val toastMessage by lazy { SingleLiveEvent<String>() }

    fun showLoader() = loaderLiveData.postValue(true)
    fun hideLoader() = loaderLiveData.postValue(false)
    fun showToast(message: String) = toastMessage.postValue(message)
    fun showErrorToast(throwable: Throwable) = errorMessageLiveData.postValue(throwable)

    protected fun handleError(throwable: Throwable) {
        throwable.printStackTrace()
        showErrorToast(throwable)
    }
}
