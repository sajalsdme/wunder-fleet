package com.example.fleet.base.fragment

import android.content.Context
import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
abstract class BaseFragment() : Fragment() {
    val TAG: String = javaClass.simpleName

    lateinit var fragmentCommunicator: FragmentCommunicator

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FragmentCommunicator) {
            fragmentCommunicator = context
        } else {
            throw ClassCastException("$context must implement BaseFragmentCommunicator")
        }
    }
}
