package com.example.fleet

import com.example.fleet.base.BaseApplication
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WunderFleet : BaseApplication()
