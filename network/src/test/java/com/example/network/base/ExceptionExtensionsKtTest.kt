package com.example.network.base

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mockito.Mockito
import retrofit2.HttpException

class ExceptionExtensionsKtTest {
    @Test
    fun parseHttpException_returns_requestException() {
        val httpException: HttpException = Mockito.mock(HttpException::class.java)

        assertThat(httpException.parseHttpException()).isInstanceOf(NetworkException::class.java)
    }
}
