package com.example.network.base

import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit

class RetrofitProviderTest {
    private val timeOutInSec = 40L
    private val baseUrl: String = "https://example.com/"

    private lateinit var retrofitClient: Retrofit

    @Before
    fun setUp() {
        retrofitClient = RetrofitProvider.getRetrofitClient(timeOutInSec, baseUrl)
    }

    @Test
    fun getRetrofitClient_returns_RetrofitClient() {
        assertThat(retrofitClient).isInstanceOf(Retrofit::class.java)
    }

    @Test
    fun retrofitClient_has_properBaseUrl() {
        assertThat(retrofitClient.baseUrl().toString()).isEqualTo(baseUrl)
    }
}
