package com.example.network.base

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class OkHttpProviderTest {
    private val timeOutInSec = 40L
    private lateinit var okHttpClient: OkHttpClient

    @Before
    fun setUp() {
        okHttpClient = OkHttpProvider.getOkHttpClient(timeOutInSec)
    }

    @Test
    fun getOkHttpClient_returns_OkHttpClient() {
        assertThat(okHttpClient).isInstanceOf(OkHttpClient::class.java)
    }

    @Test
    fun okHttpClient_returns_timeOut() {
        assertThat(okHttpClient.readTimeoutMillis).isEqualTo(timeOutInSec * 1000)
        assertThat(okHttpClient.writeTimeoutMillis).isEqualTo(timeOutInSec * 1000)
    }

    @Test
    fun okHttpClient_has_loggingInterceptor() {
        val interceptor = okHttpClient.interceptors.find {
            it is HttpLoggingInterceptor
        }
        assertThat(interceptor).isNotNull
    }
}
