package com.example.network.fleets

import com.example.network.api.ApiService
import com.example.network.base.RequestException
import com.example.network.entity.CarDetails
import com.example.network.entity.Reservation
import com.example.network.utils.TestUtil.getStringFromJsonFile
import com.example.network.utils.TestUtil.immediateExecutorService
import com.example.network.utils.TestUtil.mockResponse
import com.google.gson.Gson
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class FleetsRemoteSourceImplTest {
    private lateinit var fleetsRemoteSourceImpl: FleetsRemoteSourceImpl

    @get: Rule
    val server = MockWebServer()

    @Before
    fun setUp() {
        val okHttpClient = OkHttpClient.Builder()
            .dispatcher(Dispatcher(immediateExecutorService()))
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(server.url("/"))
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build()

        val apiService = retrofit.create(ApiService::class.java)

        fleetsRemoteSourceImpl = FleetsRemoteSourceImpl(apiService)
    }

    @Test
    fun getFleet_return_carList() {
        val totalCarsInJson = 10
        server.enqueue(mockResponse("CarList.json"))

        fleetsRemoteSourceImpl.getFleet()
            .test()
            .assertNoErrors()
            .assertComplete()
            .assertValue {
                assertThat(it.size).isEqualTo(totalCarsInJson)
                return@assertValue true
            }
    }

    @Test
    fun getFleet_return_error() {
        server.enqueue(MockResponse().setResponseCode(403).setBody(""))

        fleetsRemoteSourceImpl.getFleet()
            .test()
            .assertError {
                assertThat(it).isInstanceOf(RequestException::class.java)
                return@assertError true
            }
    }

    @Test
    fun getCarDetails_return_carDetails() {
        val jsonFileName = "CarDetails.json"
        val testCar = Gson().fromJson(getStringFromJsonFile(jsonFileName), CarDetails::class.java)

        server.enqueue(mockResponse(jsonFileName))

        fleetsRemoteSourceImpl.getCarDetails(testCar.carId)
            .test()
            .assertNoErrors()
            .assertComplete()
            .assertValue {
                assertThat(it.carId).isEqualTo(testCar.carId)
                return@assertValue true
            }
    }

    @Test
    fun getCarDetails_return_error() {
        server.enqueue(MockResponse().setResponseCode(400).setBody(""))

        fleetsRemoteSourceImpl.getCarDetails(1)
            .test()
            .assertError {
                assertThat(it).isInstanceOf(RequestException::class.java)
                return@assertError true
            }
    }

    @Test
    fun rentCar_post_success() {
        val jsonFileName = "Reservation.json"
        val reservation =
            Gson().fromJson(getStringFromJsonFile(jsonFileName), Reservation::class.java)

        server.enqueue(mockResponse(jsonFileName))

        fleetsRemoteSourceImpl.rentACar(
            url = "/",
            fleetId = reservation.carId,
            token = ""
        )
            .test()
            .assertNoErrors()
            .assertComplete()
            .assertValue {
                assertThat(it.reservationId).isEqualTo(reservation.reservationId)
                return@assertValue true
            }
    }

    @Test
    fun rentCar_return_error() {
        server.enqueue(MockResponse().setResponseCode(400).setBody(""))

        fleetsRemoteSourceImpl.rentACar(url = "/", fleetId = 1, token = "")
            .test()
            .assertError {
                assertThat(it).isInstanceOf(RequestException::class.java)
                return@assertError true
            }
    }
}
