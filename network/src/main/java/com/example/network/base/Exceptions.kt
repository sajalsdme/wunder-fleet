package com.example.network.base

const val UNKNOWN_EXCEPTION = "UNKNOWN_NETWORK_EXCEPTION"

open class NetworkException(override var message: String = "") : Exception(message)

class RequestException(var httpCode: Int = 500, override var message: String = "") : NetworkException(message)

class UnknownException : NetworkException(UNKNOWN_EXCEPTION)
