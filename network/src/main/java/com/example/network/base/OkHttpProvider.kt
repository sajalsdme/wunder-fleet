package com.example.network.base

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

object OkHttpProvider {
    private const val TIME_OUT = 45L

    private lateinit var okHttpClient: OkHttpClient

    fun getOkHttpClient(timeOut: Long = TIME_OUT): OkHttpClient {
        if (!::okHttpClient.isInitialized) {
            val okHttpBuilder = OkHttpClient.Builder()
                .readTimeout(timeOut, TimeUnit.SECONDS)
                .writeTimeout(timeOut, TimeUnit.SECONDS)
                .apply {
                    val logging = HttpLoggingInterceptor()
                    logging.level = HttpLoggingInterceptor.Level.BODY
                    addInterceptor(logging)
                }

            okHttpClient = okHttpBuilder.build()
        }
        return okHttpClient
    }
}
