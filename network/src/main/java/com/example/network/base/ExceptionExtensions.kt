package com.example.network.base

import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.core.Single
import retrofit2.HttpException

fun <T : Any> Single<T>.onException(): @NonNull Single<T> {
    return this.onErrorResumeNext { throwable: Throwable ->
        Single.create<T> { emitter ->
            println("Parser: $throwable ${throwable.javaClass}  ${throwable.message}")
            when (throwable) {
                is HttpException -> emitter.onError(throwable.parseHttpException())
                else -> emitter.onError(throwable)
            }
        }
    }
}

fun HttpException.parseHttpException(): NetworkException {
    return message?.let {
        return@let RequestException(code(), it)
    } ?: UnknownException()
}
