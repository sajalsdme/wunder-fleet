package com.example.network.base

import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitProvider {
    private const val BASE_URL = "https://s3.eu-central-1.amazonaws.com/"
    private const val TIME_OUT = 45L

    lateinit var retrofitClient: Retrofit

    fun getRetrofitClient(timeOut: Long = TIME_OUT, baseUrl: String = BASE_URL): Retrofit {
        if (!::retrofitClient.isInitialized) {
            retrofitClient = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(OkHttpProvider.getOkHttpClient(timeOut))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .build()
        }

        return retrofitClient
    }
}
