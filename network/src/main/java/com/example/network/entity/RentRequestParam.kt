package com.example.network.entity

data class RentRequestParam(
    val carId: Int
)
