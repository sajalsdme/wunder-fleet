package com.example.network.fleets

import com.example.network.api.ApiService
import com.example.network.base.onException
import com.example.network.entity.Car
import com.example.network.entity.CarDetails
import com.example.network.entity.RentRequestParam
import com.example.network.entity.Reservation
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class FleetsRemoteSourceImpl @Inject constructor(val apiService: ApiService) :
    FleetsRemoteSource {
    override fun getFleet(): Single<List<Car>> {
        return apiService.getFleetsService()
            .onException()
    }

    override fun getCarDetails(fleetId: Int): Single<CarDetails> {
        return apiService.getFleetDetails(fleetId)
            .onException()
    }

    override fun rentACar(fleetId: Int, token: String, url: String): Single<Reservation> {
        val rentRequestParam = RentRequestParam(fleetId)

        val headers = HashMap<String, String>().apply {
            put("Authorization", token)
        }

        return apiService.requestToRent(rentRequestParam = rentRequestParam, headers = headers, url = url)
            .onException()
    }
}
