package com.example.network.fleets

import com.example.network.api.ApiService
import com.example.network.entity.Car
import com.example.network.entity.CarDetails
import com.example.network.entity.Reservation
import io.reactivex.rxjava3.core.Single

interface FleetsRemoteSource {
    fun getFleet(): Single<List<Car>>
    fun getCarDetails(fleetId: Int): Single<CarDetails>
    fun rentACar(fleetId: Int, token: String, url: String = ApiService.RENT_URL): Single<Reservation>
}
