package com.example.network.api

import com.example.network.entity.Car
import com.example.network.entity.CarDetails
import com.example.network.entity.RentRequestParam
import com.example.network.entity.Reservation
import io.reactivex.rxjava3.core.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Url

interface ApiService {
    companion object {
        const val RENT_URL =
            "https://4i96gtjfia.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-rec\n" +
                "ruiting-mobile-dev-quick-rental"

        const val HEADER =
            "https://4i96gtjfia.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-rec\n" +
                "ruiting-mobile-dev-quick-rental"
    }

    @GET("wunderfleet-recruiting-dev/cars.json")
    fun getFleetsService(): Single<List<Car>>

    @GET("wunderfleet-recruiting-dev/cars/{carId}")
    fun getFleetDetails(@Path("carId") fleetId: Int): Single<CarDetails>

    @POST
    fun requestToRent(
        @Url url: String,
        @HeaderMap headers: Map<String, String>,
        @Body rentRequestParam: RentRequestParam
    ): Single<Reservation>
}
